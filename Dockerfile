FROM python:3.8-alpine

# Expose port 80
EXPOSE 80/tcp

WORKDIR /app

# Copy requirements.txt to WORKDIR
COPY requirements.txt .

# Install requirements from flask_sms.py
RUN pip install -r requirements.txt

# Copy flask_sms.py to WORKDIR
COPY flask_sms.py .
#ENV FLASK_APP="./flask_sms.py"

# Run flask_sms.py application
CMD ["python3", "./flask_sms.py"]