# AWS-Terraform-Docker
This purpose of this repo is demonstrate my knowledge with the CI/CD pipeline using AWS, Terraform and Docker. I have created a simple `Twilio` application in Python. This application gets deployed and executed on an AWS EC2 instance. All of my AWS infrastructure is defined using Terraform. This application is entirely setup using automation. 

## Demo
> Coming Soon

## How does this work?
This application is deployed when a `merge request` is made to the master branch. I have utilized GitLab's CI/CD Pipeline to assign different jobs in different stages of my pipline which includes 4 different stages: 
 1. *Validate Terraform*
 2. *Build and Push*
 3. *Staging Plan and Staging Apply*
 4. *Destroy*

### Validate Terraform
This stage is used to validate my Terraform files. In this stage of my pipeline Terraform is initialzed, formatted, then validate using the `Terraform init`, `Terraform fmt`, and `Terraform validate` commands respectively.

### Build and Push
In this stage a Docker image of my Python application is built, and pushed to my ECR repository which is defined in my terraform files.

### Staging Plan and Staging Apply
This stage outputs my Terraform plan upon succesful validate, and will apply the plan outputted by Terraform. Here is where my entire AWS infrastructure is setup. I have also defined a script `deploy/templates/ec2/user-data.sh` which is executed on my EC2 instance upon setup. A high-level description of this script is that it pulls my Docker image from my ECR repo (image that was pushed in the *build and push* stage) to my EC2 instance, and runs the Docker image. 
