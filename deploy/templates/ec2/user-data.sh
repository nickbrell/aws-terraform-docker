#!/bin/bash
sudo yum update -y

# install docker and start docker 
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service

# add our EC2 user to the Docker group
sudo usermod -aG docker ec2-user

# install Flask and Twilio
sudo pip3 install flask
sudo pip3 install twilio

 aws ecr get-login --no-include-email --region us-east-1
$(aws ecr get-login --no-include-email --region us-east-1)

# pull docker image from ecr to ec2
sudo docker image pull 554333077023.dkr.ecr.us-east-1.amazonaws.com/aws-terraform-docker:latest

# run our docker image
sudo docker run -p 80:80 554333077023.dkr.ecr.us-east-1.amazonaws.com/aws-terraform-docker