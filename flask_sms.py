# https://www.twilio.com/console
from flask import Flask, request, redirect
from twilio.twiml.messaging_response import MessagingResponse

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def sms_reply():
    # get message 'body' from incoming message
    msg_body = request.values.get('Body', None)

    # initiate twilio reply message
    reply = MessagingResponse()

    reply_str = msg_body.lower()

    # choose reply based on msg_body
    #if msg_body == 'hello':
    if 'hello' in reply_str or 'hi' in reply_str:
        reply.message("HEY!")
    #elif 'what are you doing' in msg_body:
    elif 'what are you doing' in reply_str:
        reply.message("IM SENDING YOU A MESSAGE")
    else:
        reply.message("WHAT DO YOU WANT")
    
    return str(reply)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
    
